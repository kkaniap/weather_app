import { api, LightningElement, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

const TEMPERATURE_FIELD = 'Account.Weather_temperature__c';
const DESCRIPTION_FIELD = 'Account.Weather_description__c';
const CITY_FIELD = 'Account.BillingCity';
const STREET_FIELD = 'Account.BillingStreet';
const STATE_FIELD = 'Account.BillingState';
const NAME_FIELD = 'Account.Name';
const WEBSITE_FIELD = 'Account.Website';
const PHONE_FIELD = 'Account.Phone';
const ICON_FIELD = 'Account.Weather_icon_code__c'
const weatherFields = [
    TEMPERATURE_FIELD, DESCRIPTION_FIELD, CITY_FIELD, STREET_FIELD, STATE_FIELD,
    NAME_FIELD, WEBSITE_FIELD, PHONE_FIELD, ICON_FIELD];

export default class WeatherComponent extends LightningElement {
    
    @api
    recordId;
    errorContent;
    isLoaded = false;
    temperature;
    description;
    iconCode;
    mapMarkers = [];


    @wire(getRecord, {recordId: '$recordId', fields: weatherFields})
    loadWeather({error, data}){
        if(error){
            this.errorContent = 'An error occurred while loading the weather';
        }
        else if(data){
            this.temperature = getFieldValue(data, TEMPERATURE_FIELD);
            this.description = getFieldValue(data, DESCRIPTION_FIELD).charAt(0).toUpperCase() 
                                + getFieldValue(data, DESCRIPTION_FIELD).slice(1);
            this.iconCode = getFieldValue(data, ICON_FIELD);
            this.mapMarkers = [
                {
                    location: {
                        Street: getFieldValue(data, STREET_FIELD),
                        City: getFieldValue(data, CITY_FIELD),
                        State: getFieldValue(data, STATE_FIELD)
                    },
                    title: getFieldValue(data, NAME_FIELD),
                    description: getFieldValue(data, PHONE_FIELD) + '<br/>' 
                                + getFieldValue(data, WEBSITE_FIELD),
                },
            ];
            this.changeTempIcon();
        }
        this.isLoaded = true;
    }

    get changeTempIcon(){
        (this.temperature >= 25) ? 
            this.template.querySelector('.weather-item__temp-icon').style = 'background:#ff3c2e':'';
        (this.temperature >= 20 && this.temperature < 25) ? 
            this.template.querySelector('.weather-item__temp-icon').style = 'background:#fff06e':'';
        (this.temperature >= 10 && this.temperature < 20) ? 
            this.template.querySelector('.weather-item__temp-icon').style = 'background:#14d6fc':'';
        (this.temperature >= 0 && this.temperature < 10) ? 
            this.template.querySelector('.weather-item__temp-icon').style = 'background:#1488fc':'';
        (this.temperature < 0) ?
            this.template.querySelector('.weather-item__temp-icon').style = 'background:#1461fc':'';
    }

    get changeWeatherIcon(){
        return ' http://openweathermap.org/img/wn/' + this.iconCode + '@2x.png';
    }
}