public with sharing class OpenWeatherServices {
    
    private static final Http http = new Http();
    private static final String accessToken = '&appid=fc3d47ffc2552e46aec52af4209a5947';
    private static final String apiAddress = 'http://api.openweathermap.org/data/2.5/weather?q=';
    private static final String metricUnits = '&units=metric';

    public static Map<String, OpenWeatherItem> getWeather(List<String> cities){

        Map<String, OpenWeatherItem> resultsWeather = new Map<String, OpenWeatherItem>();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');

        for(String city : cities){
            city = city.replaceAll('\\s+', '%20');
            request.setEndpoint(apiAddress + city + accessToken + metricUnits);
            HttpResponse response = http.send(request);

            if(response.getStatusCode() == 200){
                OpenWeatherItem item = new OpenWeatherItem();
                Map<String, Object> results =
                    (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                Map<String, Object> weather = 
                    (Map<String, Object>)((List<Object>)results.get('weather')).get(0);
                Map<String, Object> main = (Map<String, Object>)results.get('main');

                item.iconCode = (String)weather.get('icon');
                item.description = (String)weather.get('description');
                item.conditions = (String)weather.get('main');
                item.temperature = (Decimal)main.get('temp');
                resultsWeather.put(city.replaceAll('%20', ' '), item);
            }
            else if(response.getStatusCode() == 429){
                throw new ToManyCallException('You can only make 60 calls per minute');          
            }
        }
        return resultsWeather;
    }
}
