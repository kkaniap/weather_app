global class OpenWeatherRunner implements Schedulable{
    global void execute(SchedulableContext ctx) {
        OpenWeatherBatch batch = new OpenWeatherBatch();
        Database.executeBatch(batch, 10);
    }
}
