public with sharing class OpenWeatherItem {
    public Decimal temperature {get; set;}
    public String description {get; set;}
    public String iconCode {get; set;}
    public String conditions {get; set;}
}
