global class OpenWeatherBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts{
    //s
    Set<String> checkSet = new Set<String>();
    Map<String, OpenWeatherItem> weather = new Map<String, OpenWeatherItem>();
    private static Boolean incomplete = false;
    private static Id scheduleId;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        // if(incomplete){
        //   
        // }
        // else{
        //     return Database.getQueryLocator('SELECT ID, BillingCity FROM Account');
        // }
        return Database.getQueryLocator('SELECT ID, BillingCity FROM Account');
    }

    global void execute(Database.BatchableContext bc, List<Account> scope){
        List<String> citiesToCheck = new List<String>();
        List<Account> accounts = new List<Account>();
    
        //Checks if the weather exists for the city
        //if not, adds a city to the list to be checked
        for(Account account : scope){
            if(checkSet.add(account.BillingCity) && account.BillingCity != null){
                citiesToCheck.add(account.BillingCity);
            }
        }

        //If the list is greater than zero,
        //take the weather for new cities
        if(citiesToCheck.size() > 0){
            try{
                Map<String, OpenWeatherItem> newWeather = OpenWeatherServices.getWeather(citiesToCheck);
                weather.putAll(newWeather);

                //Update weather fields in Account
                for(Account account : scope){
                    if(account.BillingCity != null){
                        account.Weather_temperature__c = weather.get(account.BillingCity).temperature;
                        account.Weather_description__c = weather.get(account.BillingCity).description;
                        account.Weather_icon_code__c = weather.get(account.BillingCity).iconCode;
                        account.Weather_conditions__c = weather.get(account.BillingCity).conditions;
                        accounts.add(account);
                    }
                }
            }catch(ToManyCallException e){
                System.abortJob(bc.getJobId());
                if(scheduleId != null){
                    System.abortJob(scheduleId);
                }

                OpenWeatherRunner runner = new OpenWeatherRunner();
                String hour = String.valueOf(Datetime.now().hour());
                String min = String.valueOf(Datetime.now().minute() + 1); 
                String ss = String.valueOf(Datetime.now().second());
                String cron = ss + ' ' + min + ' ' + hour + ' * * ?';
                scheduleId = System.schedule('Weather update - incomplete', cron, runner);
            }
        }
        update accounts;
    }    

    global void finish(Database.BatchableContext bc){
    }    
}